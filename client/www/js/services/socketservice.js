angular.module('eassist.socketservice', ['lbServices', 'ionic'])
  .service('socket', function (LoopBackAuth, $rootScope,  $q) {

    var socket;
    
    var data = {};
    data.authenticated = false;
    data.roomId = undefined;
    data.userId = undefined;
    data.msgCount = 0;

    return {
      authenticate: function () {
        var deferred = $q.defer();
        var accessTokenId = LoopBackAuth.accessTokenId;
        data.userId = LoopBackAuth.currentUserId;
        if (socket == null || socket == undefined) {
          socket = io.connect('http://localhost:1337',
          // socket = io.connect('https://eassist.au-syd.mybluemix.net',
            {
              'forceNew': true,
              'reconnection': true,
              'reconnectionDelay': 1000,
              'reconnectionDelayMax': 5000,
              'reconnectionAttempts': Infinity
            });
          socket.on('connect', function () {
            socket.emit('authentication', { accessTokenId: accessTokenId, userId: data.userId });
            socket.on('authenticated', function () {
            data.authenticated = true;              
              console.log('User is authenticated');
              $rootScope.socketData = data;
            });
            socket.on("room:data", function(res){
              data.roomId = res.roomId;
              data.msgCount = res.msgCount;
              // console.log(data.roomId);
              // socket.emit('join', {roomId: data.roomId, userId: data.userId});
              $rootScope.socketData = data;
              deferred.resolve();
            });
            
          });
        }
        return deferred.promise;
      },
      getRoomId: function(){
        console.log("In Method getRoomId");
        console.log(data.roomId);
        return data.roomId;
      },
      disconnect: function () {
        socket.io.disconnect();
        delete socket;
        socket = null;

      },
      join: function (roomId) {
        socket.emit('join', { roomId: roomId });
      },
      leave: function () {
        socket.emit('leave', { roomId: roomId });
      },
      on: function (eventName, callback) {
        socket.on(eventName, function () {
          var args = arguments;
          $rootScope.$apply(function () {
            callback.apply(socket, args);
          });
        });
      },
      emit: function (eventName, data, callback) {
        socket.emit(eventName, data, function () {
          var args = arguments;
          $rootScope.$apply(function () {
            if (callback) {
              callback.apply(socket, args);
            }
          });
        })
      }
    };
  })