// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
angular.module('eassist', ['eassist.account','eassist.dev','eassist.chat','eassist.login','eassist.register','eassist.socketservice', 'ionic','lbServices','bd.timedistance'])

    /*.run(function ($ionicPlatform) {
     $ionicPlatform.ready(function () {
     // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
     // for form inputs)
     if (window.cordova && window.cordova.plugins.Keyboard) {
     cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
     }
     if (window.StatusBar) {
     StatusBar.styleDefault();
     }
     });
     })*/

    .run(function (Customer) {
        //Check if User is authenticated
        if (Customer.getCachedCurrent() == null) {
            Customer.getCurrent();
        }
    })
    .config(function ($stateProvider, $urlRouterProvider, $httpProvider) {
        $stateProvider
            .state('login', {
                url: '/login',
                templateUrl: 'templates/login.html',
                controller: 'LoginCtrl'
            })
            .state('register', {
                url: '/register',
                templateUrl: 'templates/register.html',
                controller: 'RegisterCtrl'
            })
            .state('tabs', {
                url: '/tab',
                abstract: true,
                templateUrl: 'templates/tabs.html'
            })
            .state('tabs.chat', {
                url: '/chat',
                views: {
                    'chat-tab': {
                        templateUrl: 'templates/chat.html',
                        controller: 'ChatTabCtrl'
                    }
                }
            })
            .state('tabs.dev', {
                url: '/dev',
                views: {
                    'dev-tab': {
                        templateUrl: 'templates/dev.html',
                        controller: 'DevCtrl'
                    }
                }
            })
            .state('tabs.account', {
                url: '/account',
                views: {
                    'account-tab': {
                        templateUrl: 'templates/account.html',
                        controller: 'AccountCtrl'
                    }
                }
            });

        $urlRouterProvider.otherwise('/login');

        $httpProvider.interceptors.push(function ($q, $location) {
            return {
                responseError: function (rejection) {
                    console.log("Redirect");
                    if (rejection.status == 401 && $location.path() !== '/login' && $location.path() !== '/register') {
                        $location.nextAfterLogin = $location.path();
                        $location.path('/login');
                     }
                    return $q.reject(rejection);
                }
            };
        });

        
        $httpProvider.interceptors.push(function() {
            return {
              request: function(req) {
                // Transform **all** $http calls so that requests that go to `/`
                // instead go to a different origin, in this case localhost:3000
                if (req.url.charAt(0) === '/') {
                  // req.url = 'https://eassist.au-syd.mybluemix.net' + req.url;
                  req.url = 'http://localhost:1337' + req.url;
                  // and make sure to send cookies too
                  req.withCredentials = true;
                }
         
                return req;
              }
            };
        });
    })
;