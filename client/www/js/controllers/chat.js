angular.module('eassist.chat', ['lbServices'])
    .controller('ChatTabCtrl', function ($scope, $location, socket, Customer, $rootScope, Message, $ionicScrollDelegate) {
        $scope.currentUser = Customer.getCurrent();
        $scope.message = {};
        $scope.messages = [];
        $scope.showLoading = true;
        $scope.roomId;
        socket.authenticate().then(function() {
            // console.log($rootScope.socketData);
            // $scope.roomId = $rootScope.socketData.roomId;
            // if($scope.roomId)
            //     $scope.loadRoomData();
        });

        $rootScope.$watch('socketData', function(newValue, oldValue) {
          // console.log(newValue);
          // console.log(oldValue);
          // if(newValue != oldValue){
            if(newValue){
                $scope.roomId = $rootScope.socketData.roomId;
                $scope.showLoading = false;
                $scope.loadRoomData($rootScope.socketData.msgCount);
            }            
          // }
        });

        $scope.loadRoomData = function (count) { 
            var skipCount = 0;
            if(count > 30) {
                skipCount = count - 30;
            }
            Message.find( 
                {
                    filter: {
                        order: 'posted_at DESC',
                        skip: skipCount,
                        // limit: 26,
                        where: { roomId: $scope.roomId }
                    }
                },
                function (msgs) {   
                // console.log(msgs);                              
                    $scope.messages = msgs;
                    $scope.messages.reverse();
                    $ionicScrollDelegate.scrollBottom();
                    socket.emit('join', {roomId: $scope.roomId , userId: $scope.currentUser.id});
                },
                function (err) {
                    console.error(err);
                });                
        }

        socket.on('send:message', function (msg) {
            console.log('message: ' + msg);
            $scope.messages.push(msg);
            $ionicScrollDelegate.scrollBottom();
        });

        /**
         * showAlert()
         * @param {string} data
         * shows a popup with the error
         */
        $scope.showAlert = function (data) {
            $ionicPopup.alert({
                title: 'Error',
                template: data
            })
        };

        /**
         * @name isUser()
         * @description
         * Checks whether the message passed belongs to user or bot
         */

        $scope.isUser = function (message) {
            if (message.userId == $scope.currentUser.id)
                // if user align right
                return 'rightAlignMessage';
                // if bot align left
            return 'leftAlignMessage';
        }

        $scope.sendButtonText = function(obj){
            // console.log($event.target);            
            $scope.messageConent = obj.askfor;
            $scope.sendMessage();
        }

        /**
         * @name sendMessage()
         * @description
         * Send message to server via socket
         */
        $scope.sendMessage = function () {
            $scope.message.templateName = "generic";
            $scope.message.templateContent = {};
            $scope.message.templateContent.text = $scope.messageConent;
            $scope.message.userId = $scope.currentUser.id;
            // $scope.message.roomId = $scope.roomId;
            $scope.message.posted_at = new Date();

            socket.emit('send:message', {
                message: $scope.message
            });

            $scope.messages.push($scope.message);
            $ionicScrollDelegate.scrollBottom();
            $scope.message = {};
            $scope.messageConent = "";
        };
    })