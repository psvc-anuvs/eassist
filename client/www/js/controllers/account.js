angular.module('eassist.account', ['lbServices', 'ionic'])
    .controller('AccountCtrl', function ($scope, $location, Customer, socket) {
        $scope.currentUser = Customer.getCurrent();

        /**
         * @name logout()
         * logout user and redirect to the login page
         */
        $scope.logout = function () {
                       
            Customer.logout(function () {                                
                socket.disconnect();
                $location.path('/login');
            });
        }

    });