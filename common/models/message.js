module.exports = function(Message) {

// Message.sendmessage = function(message,cb){
// 		 var chatbot = Message.app.chatbot;
// 		 Message.create({'content':message.content,
// 		 				 'userId':message.userId,
// 		 				 'roomId':message.roomId,
// 		 				 'posted_at':new Date()},
// 	      function(err,mess){
// 	      	console.log(mess);
// 	      		Message.findById(mess.id, function(err,data){
// 	      			var reply = chatbot.reply(message.userId, data.content);
// 	      			Message.create({'content':reply,
// 		 				 'userId':'bot1',
// 		 				 'roomId':message.roomId,
// 		 				 'posted_at':new Date()},
// 		      			function(err,mess){
// 		      				Message.app.io.in(mess.roomId).emit('send:message',reply);
// 		      			});
    				
// 	    			cb();
//     			});
	 
// 	    });
// 	}
 
// 	Message.remoteMethod('sendmessage', {
// 		accepts: [
// 	      {arg: 'message', type: 'object', http: { source: 'body' }}
// 	     ],
// 	    returns: {arg: 'success', type: 'boolean'},
// 	    http: {path:'/sendmessage', verb: 'post'}
// 	  });

	Message.observe('after save', function(ctx, next) {
		var app = Message.app;
		var message = ctx.instance;
		var Room = app.models.Room;			
	  if (ctx.isNewInstance) {	   
	  // console.log(message);    
	    Room.findById(message.roomId, function(err, room){ 
	    	// console.log(room);
			room.msgCount++;
			room.save();
	    });
	  }
	  next();
	});
 
};
