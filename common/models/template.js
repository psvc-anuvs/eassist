var stringjs = require('string');
const vm = require('vm');

module.exports = function(Template) {

	Template.prototype.generateQuestion = function(enableSteps, WithAnswer, difficultyLevel){
		// console.log(this.content);
		var context = new vm.createContext(this.defaultInput);
		var script = new vm.Script(this.solution);

		var question = stringjs(this.content).template(this.defaultInput).s;
		// console.log(question);
		var answer =  script.runInContext(context);
		var data = {
			question : question,
			answer: answer
		};
		return data;
	}

};
