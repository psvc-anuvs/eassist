//var rand = require("random-key");
var loopback = require('loopback');
module.exports = function(Customer) {

    Customer.observe('after save', function(ctx, next) {
        var app = Customer.app;
        var customer = ctx.instance;
        var Room = app.models.Room;
        var Advisor = app.models.Advisor;
        if (ctx.isNewInstance) {
            console.log('User (id) %s created', customer.id);
            Room.create({ name: customer.id + "_bot" }, function(err, room) { //roomId: rand.generate(), 
                var roomId = room.roomId;
                customer.joinedrooms.add(room, function(err, res) {
                    console.log('Customer (id) %s has joined room %s ', customer.id, roomId);
                });
                app.bot.joinedrooms.add(room, function(err, res) {
                    console.log('Advisor (id) %s has joined room %s', app.bot.botId, roomId);
                });
            });
        }
        next();
    });

    // Customer.observe('access', function logQuery(ctx, next) {
    //     var loopbackContext = loopback.getCurrentContext();
    //     if (loopbackContext) {
    //         console.log("currentUser "+loopbackContext.get('currentUser'));
    //     }
    //     // console.log('Accessing %s matching %s', ctx.Model.modelName, ctx.query.where);
    //     next();
    // });

    // Customer.prototype.getCurrentUser = function(){
    // 	var loopbackContext = loopback.getCurrentContext();
    //     if (loopbackContext) {
    //         console.log("currentUser "+loopbackContext.get('currentUser'));
    //     }
    //     return loopbackContext.get('currentUser');
    // }
};
