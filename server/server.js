var loopback = require('loopback');
var boot = require('loopback-boot');
var rivescript = require("rivescript");
require( "console-stamp" )( console, {
    // pattern : "dd/mm/yyyy HH:MM:ss.l" ,
    metadata: function () {
        return ("[" + process.memoryUsage().rss + "]");
    },
    colors: {
        stamp: "yellow",
        label: "white",
        metadata: "green"
    }
} );

var app = module.exports = loopback();
app.io = require('socket.io')();
// app.chatbot = new RiveScript();


// app.use(loopback.token());
// app.use(loopback.context());
// function inject(ctx, next) {
//   var options = hasOptions(ctx.method.accepts) && (ctx.args.options || {});
//   if(options) {
//     options.remoteCtx = ctx;
//     ctx.args.options = options;
//   }
//   next();
// }

// app.remotes().before('*.*', inject);

// app.remotes().before('*.prototype.*', function(ctx, instance, next) {
//   inject(ctx, next);
// });

// // unfortunately this requires us to add the options object
// // to the remote method definition
// app.remotes().methods().forEach(function(method) {
//   if(!hasOptions(method.accepts)) {
//     method.accepts.push({
//       arg: 'options',
//       type: 'object',
//       injectCtx: true
//     });
//   }
// });

// function hasOptions(accepts) {
//   for (var i = 0; i < accepts.length; i++) {
//     var argDesc = accepts[i];
//     if (argDesc.arg === 'options' && argDesc.injectCtx) {
//       return true;
//     }
//   }
// }

app.use(function setCurrentUser(req, res, next) {
  if (!req.accessToken) {
    return next();
  }
  // console.log(req.accessToken.userId);
  app.models.Customer.findById(req.accessToken.userId, function(err, user) {
    if (err) {
      return next(err);
    }
    // console.log(user);
    if (!user) {
      return next(new Error('No user with this access token was found.'));
    }
    res.locals.currentUser = user;
    var loopbackContext = loopback.getCurrentContext();
    if (loopbackContext) {
      // console.log("setting currentUser to", user);
      loopbackContext.set('currentUser', user);
      // console.log(loopbackContext.get('currentUser'));
    }
    next();
  });
});



// Bootstrap the application, configure models, datasources and middleware.
// Sub-apps like REST API are mounted via boot scripts.
boot(app, __dirname, function(err) {
  if (err) throw err;

  //Use Bluemix host and port ...  
  var host = process.env.VCAP_APP_HOST || 'localhost';
  var port = process.env.VCAP_APP_PORT || 1337;
  
  app.set('host', host);
  app.set('port', port);
  
  // start the server if `$ node server.js`
  if (require.main === module){
    //app.start();
    app.io.attach(app.start());    
  }
});

app.start = function() {
  // start the web server
  return app.listen(function() {
    app.emit('started');
    console.log('Web server listening at: %s', app.get('url'));
  });
};
