module.exports = function(app) {

    require('socketio-auth')(app.io, {
        authenticate: function(socket, value, callback) {
                // console.log('In authenticated function');
                var AccessToken = app.models.AccessToken;
                var Customer = app.models.Customer;
                var Message = app.models.Message;
                var Room = app.models.Room;
                //get credentials sent by the client
                var token = AccessToken.find({
                    where: {
                        and: [{ userId: value.userId }, { id: value.id }]
                    }
                }, function(err, tokenDetail) {
                    if (err) throw err;
                    if (tokenDetail.length) {
                        console.log("user authenticated " + value.userId);
                        Customer.findById(tokenDetail[0].userId, { include: { relation: 'joinedrooms' } },
                            function(err, data) {
                                if (err)
                                    console.error(err);

                                if (data) {
                                    // console.log(data);
                                    app.chatbot.currentUser = data;
                                    var roomData = {};
                                    roomData.roomId = data.joinedrooms()[0].roomId;
                                    roomData.msgCount = data.joinedrooms()[0].msgCount;
                                    socket.emit("room:data", roomData);
                                }
                            });
                        callback(null, true);
                    } else {
                        callback(null, false);
                    }
                }); //find function..    
            } //authenticate function..
    });

    app.io.on('connection', function(socket) {
        var roomId;
        console.log('Client connected!');
        var sockets = socket.server.sockets.sockets;
        console.log('Number of connections ' + Object.keys(sockets).length);

        socket.on('join', function(data) {
            roomId = data.roomId;
            socket.join(roomId);
            var Message = app.models.Message;
            var chatbot = app.chatbot;
            var templateContent = {
                text: "introduce bot"
            };
            var msgData = {
                userId: data.userId,
                // content: "introduce bot",
                templateContent: templateContent,
                roomId: roomId
            }
            // console.log("In socket join "+ msgData.userId + " "+ msgData.content +" "+ msgData.roomId);
            chatbot.getReplyAndsendMessage(msgData);
        });

        socket.on('leave', function(data) {
            socket.leave(roomId); // We are using room of socket io
            console.log('user left');
            console.log('Number of connections ' + Object.keys(sockets).length);
        });

        socket.on('send:message', function(data) {
            var chatbot = app.chatbot;
            var Message = app.models.Message;
            var message = data.message;
            Message.create({
                    'templateContent': message.templateContent,
                    'userId': message.userId,
                    'roomId': roomId,
                    'posted_at': message.posted_at
                },
                function(err, res) {
                    if (err)
                        console.error(err);
                    if (res) {
                        chatbot.getReplyAndsendMessage(res);
                    }
                });
        });

        socket.on('disconnect', function() {
            console.log('user disconnected');
            socket.disconnect();
            console.log('Number of connections ' + Object.keys(sockets).length);
        });
    });
};
