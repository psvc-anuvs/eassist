var async = require('async');

module.exports = function(app) {
    var Category = app.models.Category;
    var Topic = app.models.Topic;
    var ProblemType = app.models.ProblemType;
    var Template = app.models.Template;
    var Definition = app.models.Definition;
    var Formula = app.models.Formula;

    var formula, definition, template, problemType, topic, category;
    async.series([
            function createFormula(done) {

                var equationFromLoss = "CP = { 100/(100-LOSS%) } * SP";
                var explanationFromLoss = "CP Stands for Cost Price, SP Stands for Selling Price";
                var exampleFromLoss = "Find CP when, SP = Rs. 51.70, LOSS = 12%, " +
                    " CP = { ( 100 / 88) * 51.70 } = 58.75";

                Formula.create({ equation: equationFromLoss, explanation: explanationFromLoss, example: exampleFromLoss }, function(err, res) {
                    formula = res;
                    done(err, res);
                });
            },
            function createDefinition(done) {
                var defn = {
                    name: "Cost Price",
                    description: "THE PRICE AT WHICH ARTICLE IS PURCHASED. ABBREVATED AS C.P."
                };
                Definition.create(defn, function(err, res) {
                    definition = res;
                    done(err, res);
                });

            },
            function createTemplate(done) {
                var contentForCPFromLoss = " A Selling Price of an article is Rs. {{sp}}, and " +
                    "the vendor makes loss of {{loss}}% .Find the cost price " +
                    "for the article ? ";
                var difficultForCPFromLoss = "easy";
                var solutionForCPFromLoss = "(100 / (100 - loss)) * sp;";
                var input = {
                	sp: 85,
                	loss: 25
                };
                Template.create({ content: contentForCPFromLoss, difficulty: difficultForCPFromLoss, solution: solutionForCPFromLoss, defaultInput: input },
                    function(err, res) {
                        template = res;
                        done(err, res);
                    });
            },
            function createProblemType(done) {
                ProblemType.create({ name: "Finding Cost Price From Loss", difficulty: "easy"}, function(err, res) {
                    problemType = res;
                    problemType.definitions.create(definition);
                    problemType.templates.create(template);
                    problemType.formulas.create(formula);
                    problemType.save(done);
                    // console.log("problem Type is " + problemType);
                    // done(err, res);
                });
            },
            function createTopic(done) {
                Topic.create({ name: "Profit and Loss" }, function(err, res) {
                    topic = res;
                    topic.problems.create(problemType);
                    topic.save(done);                    
                });
            },
            function createCategory(done) {
                Category.create({ name: "Quantitative Apptitude" }, function(err, res) {
                    category = res;
                    category.topics.create(topic);
                    category.save(done);                    
                });
                Category.create({ name: "Logical" }, function(err, res) {
                    
                });
                Category.create({ name: "English Reasoning" }, function(err, res) {
                    
                });
            }

        ],
        function(err) {

            if (err != null)
                console.log(err);

            
            Category.findById(category.categoryId, {
                include: {
                    relation: 'topics'//,
                    // scope: { // fetch 1st "page" with 5 entries in it
                    //     include: ["problems"],
                    //     skip: 0,
                    //     limit: 5
                    // }
                }
            }, function(err, res) {

                if (err != null)
                    console.log(err);

				//console.log(res);//[0].problems()[0].templateList[0]);
                // console.log(res.topics()[0].problems()[0].templateList[0].generateQuestion(false, false, false));
                // console.log(res);
            });

            Topic.findOne({
                    include: {
                        relation: 'problems',
                        scope: {
                            fields: ['typeId']
                        }                       
                    },
                    where: { name: { like: "Profit and Loss" } }
                },
                function(err, res) {
                    if (err != null)
                    console.log(err);

                if (res != null)
                    console.log(res);
                });


        });
}