var RiveScript = require("rivescript");
var loopback = require('loopback');

module.exports = function(app) {

    var AsyncBot = function() {
        var self = this;
        self.brain = new RiveScript();
        self.currentUser = {};
        self.currentCategory = {};
        self.currentTopic = {};
        self.currentProblem = {};
        // var userId;

        // Load the replies and process them.
        self.brain.loadDirectory("./brain", loading_done, loading_error);

        function loading_done(batch_num) {
            console.log("Batch #" + batch_num + " has finished loading!");

            // Now the replies must be sorted!
            self.brain.sortReplies();
            app.chatbot = self;
        }

        function loading_error(batch_num, error) {
            if (error != undefined)
                console.log("Error when loading brain: " + error);
        }

        function createAndSendMsg(data) {
            var Message = app.models.Message;
            data.userId = "bot1";
            data.posted_at = new Date();
            if (data.templateContent.text.trim() != "") {
                Message.create(data,
                    function(err, msg) {
                        if (err)
                            console.error(err);

                        console.log(msg);
                        app.io.in(data.roomId).emit('send:message', msg);
                    });
            }
        }

        self.getCategories = function(callback) {
            var Category = app.models.Category;
            Category.find(function(err, res) {
                // if (err)
                //     callback.call(this, err);
                // else
                console.log("in get Category");
                callback.call(this, err, res);
            });
        }

        self.getTopics = function(catName, callback) {
            var Topic = app.models.Topic;
            var Category = app.models.Category;
            // console.log(catName);
            var pattern = new RegExp('.*' + catName + '.*', "i"); /* case-insensitive RegExp search */
            Category.findOne({
                    include: {
                        relation: 'topics'
                    },
                    where: { name: { like: pattern } }
                },
                function(err, res) {
                    console.log(res);
                    self.currentCategory = res;
                    if (err)
                        callback.call(this, err);
                    else
                        callback.call(this, null, res);
                });
        }

        self.getProblem = function(topicName, callback) {
            var Topic = app.models.Topic;
            var ProblemType = app.models.ProblemType;
            var pattern = new RegExp('.*' + topicName + '.*', "i"); /* case-insensitive RegExp search */
            Topic.findOne({
                    include: {
                        relation: 'problems',
                        scope: {
                            fields: ['typeId']
                        }
                    },
                    where: { name: { like: pattern } }
                },
                function(err, res) {
                    console.log(res);
                    self.currentTopic = res;
                    var problems = res.problems();
                    var probIds = [];
                    for (var i in problems) {
                        probIds.push(problems[i].typeId);
                    }
                    var problemId = probIds[Math.floor(Math.random() * probIds.length)];
                    ProblemType.findById(problemId, function(err, res) {
                        self.currentProblem = res;
                        if (err)
                            callback.call(this, err);
                        else
                            callback.call(this, null, res);
                    });

                });
        }

        // self.brain.setSubroutine("getCategoriesHelper", function(brain, args) {
        //     // return new brain.Promise(function(resolve, reject) {
        //     getCategories(args.join(' '), function(err, data) {
        //         // var loopbackContext = app.loopback.getCurrentContext();
        //         // console.log(loopbackContext);
        //         // if (loopbackContext) {
        //         //     console.log("currentUser " + loopbackContext.get('currentUser'));
        //         // }
        //         console.log(app.ctx.options.remoteCtx.accessToken.userId);
        //         // var Customer = app.models.Customer;
        //         // console.log(Customer.getCurrentUser());
        //         // var userId = brain.currentUser();
        //         if (err) {
        //             console.error(err);
        //             // reject(error);
        //         } else {
        //             console.log(data);
        //             self.sendMessage(userId, data);
        //             // resolve(data);
        //         }
        //     });
        //     // });
        // });

        self.getReplyAndsendMessage = function(data) {
            console.log("In socket join ");
            var reply = getReply(data);
            var Message = app.models.Message;
            var msgData = {};
            msgData.userId = data.userId;
            msgData.templateName = "generic";
            msgData.templateContent = {};
            msgData.templateContent.text = reply;
            msgData.roomId = data.roomId;
            createAndSendMsg(msgData);
        };

        self.sendMessage = function(data) {
            console.log("in send msg");
            var Customer = app.models.Customer;
            var Message = app.models.Message;
            Customer.findById(data.userId, { include: { relation: 'joinedrooms' } },
                function(err, res) {
                    console.log("in get customer res");
                    if (err)
                        console.error(err);

                    if (res) {
                        data.roomId = res.joinedrooms()[0].roomId;
                        createAndSendMsg(data);
                    }
                    console.log("out get customer res");
                });

            console.log("out send msg");
        };

        // This is a function for a user requesting a reply. It just proxies through
        // to RiveScript.
        self.getReply = function(data) {
            // userId = userId;
            // When we call RiveScript's getReply(), we pass `self` as the scope
            // variable which points back to this AsyncBot object. This way the
            // object macro can call `this.sendMessage()` to asynchronously send
            // a response to the user.
            // console.log("In sget reply "+ data.userId + " "+ data.content +" "+ data.roomId);
            return self.brain.reply(data.userId, data.templateContent.text, self);
        }
    };

    AsyncBot();
}
